using System;
using UnityEngine;

namespace Question
{
    [Serializable]
    public class Pregunta
    {
        public int id;
        public string type;
        public Sprite picture;
        public string answer;

        public Pregunta(int _id, string _type, Sprite _picture, string _answer)
        {
            id = _id;
            type = _type;
            picture = _picture;
            answer = _answer;
        }
    }
}