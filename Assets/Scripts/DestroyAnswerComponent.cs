using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAnswerComponent : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GameObject _answers = GameObject.Find("Answers");
        if(_answers != null)
            Destroy(GameObject.Find("Answers"));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
