using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SaveResults : MonoBehaviour
{
    [SerializeField] Text _lastResultText;
    string _daltonico;

    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.GetInt("resultados") != 0)
        {
            if (PlayerPrefs.GetInt("daltonico") == 1) _daltonico = "Daltonico";
            else _daltonico = "No daltonico";

            _lastResultText.text = "Ultimo Resultado:" + PlayerPrefs.GetInt("resultados") + "/25\n" + _daltonico;

        }
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(PlayerPrefs.GetInt("daltonico"));
    }
}
