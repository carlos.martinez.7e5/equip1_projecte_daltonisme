using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultManager : MonoBehaviour
{
    GameObject _data;
    [SerializeField] Text _resultText;

    // Start is called before the first frame update
    void Start()
    {
        _data = GameObject.Find("Answers");
        int acertadas = _data.GetComponent<FinalAnswers>().CorrectAnswers;

        _resultText.text = "Resultado: " + acertadas + "/25";
        PlayerPrefs.SetInt("resultados", acertadas);

        int dalt; // 0 = no, 1 = daltonico. Como no guarda bool, nos lo inventamos

        if (acertadas < 13) dalt = 1; 
        else dalt = 0;

        PlayerPrefs.SetInt("daltonico", dalt);
        Debug.Log(PlayerPrefs.GetInt("daltonico"));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
