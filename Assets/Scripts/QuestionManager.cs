using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Question;

public class QuestionManager : MonoBehaviour
{
    [SerializeField]GameObject _questions;
    [SerializeField] Image _testImage;
    [SerializeField] Button _nextQuestionButton = null;
    [SerializeField] Text _nextQuestionText;
    [SerializeField] Text _questionText;
    [SerializeField] Text _questionNumberText;

    int _questionNumber;
  
    public Data resultats; //Variable data, donde se guardar�n los resultados.
    [SerializeField] private Text respostaUsuari; //El texto, la respuesta del usuario
    Question.Pregunta currentQuestion; //La pregunta que el usuario est� respondiendo actualmente.

    // Start is called before the first frame update
    void Start()
    {
        resultats = new Data();

        _questionNumber = 0;
        _questionNumberText.text = _questionNumber + 1 + "/25";
        _testImage.sprite = _questions.GetComponent<PreguntaManager>().arrayPreguntas[_questionNumber].picture;

        _nextQuestionButton.onClick.AddListener(() => { NextQuestion(); });
    }

    private void NextQuestion()
    {
        //Debug.Log("Has escrito el numero " + respostaUsuari.text);

        currentQuestion = _questions.GetComponent<PreguntaManager>().arrayPreguntas[_questionNumber];
        //Debug.Log("La respuesta de la lamina " + currentQuestion.id + " es " + currentQuestion.answer);
        if (respostaUsuari.text == currentQuestion.answer) resultats.preguntasAcertadas++;
        //Debug.Log(resultats.preguntasAcertadas);
        Debug.Log("Hasta ahora acertaste " + resultats.preguntasAcertadas);
        _questionNumber++;

        _questionNumberText.text = _questionNumber + 1 + "/25";

        if (_questionNumber <= _questions.GetComponent<PreguntaManager>().arrayPreguntas.Length - 1)
        {
            ChangeQuestionText();
            _testImage.sprite = _questions.GetComponent<PreguntaManager>().arrayPreguntas[_questionNumber].picture;

            if (_questionNumber == _questions.GetComponent<PreguntaManager>().arrayPreguntas.Length - 1)
                _nextQuestionText.text = "Finalizar Test";
        }
        else
        {
            //DontDestroyOnLoad(resultats);
            SceneManager.LoadScene("EndCanvasScene");
        }
        //Debug.Log("Tal");
        respostaUsuari.text = "";
    }

    private void ChangeQuestionText()
    {
        switch (_questions.GetComponent<PreguntaManager>().arrayPreguntas[_questionNumber].type)
        {
            case "Numeric":
                _questionText.text = "�Qu� numero ves?";
                break;
            case "Lines":
                _questionText.text = "�Cuantas linias ves?";
                break;
        }
    }
}
