using UnityEngine;

public class SceneChangeManager : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] public Boton[] arrayBotones = new Boton[1];

    void Start()
    {
        foreach(Boton boton in arrayBotones)
        {
            boton.EventButton();
        }

    }
}