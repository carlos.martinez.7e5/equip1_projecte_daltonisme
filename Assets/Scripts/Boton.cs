using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

    [Serializable]
    public class Boton
    {
    public Button _UIButton;
    public string NextSceneName;

    public Boton(Button UIButton, string nextSceneName)
    {
        _UIButton = UIButton;
        NextSceneName = nextSceneName;
    }


    public void EventButton()
    {
        _UIButton.onClick.AddListener(() => { GoToScene(); });
    }
    private void GoToScene()
    {
        SceneManager.LoadScene(NextSceneName);
    }

    void Start()
    {

    }
 
}
