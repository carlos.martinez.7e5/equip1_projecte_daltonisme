using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitApplication : MonoBehaviour
{
    public void Quit()
    {
        //Solo funcionara en la aplicación construida, no en el editor.
        Application.Quit(); 

        //Para que funcione en el editor:
        //UnityEditor.EditorApplication.isPlaying = false;
    }
}
