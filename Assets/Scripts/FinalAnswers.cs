using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalAnswers : MonoBehaviour
{
    [SerializeField]
    GameObject _sceneManager;
    public int CorrectAnswers;

    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(this);
    }

    // Update is called once per frame
    void Update()
    {
        if (_sceneManager != null) CorrectAnswers = _sceneManager.GetComponent<QuestionManager>().resultats.preguntasAcertadas;
    }
}
