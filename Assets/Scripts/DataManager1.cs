using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class Data : MonoBehaviour
{
    public bool daltonico;
    public int preguntasAcertadas;

    public Data(Data d)
    {
        d.daltonico = this.daltonico;
        d.preguntasAcertadas = this.preguntasAcertadas;
    }

    public Data()
    {
        daltonico = false;
        preguntasAcertadas = 0;
    }
}


//De momento no se usa, encontramos una manera m�s sencilla con el playerPref
public static class DataManager
{
    public static void Save(Data data)
    {
        Data testData = new Data(data);
        string path = Application.persistentDataPath + "/test.save";
        FileStream fS = new FileStream(path, FileMode.Create);
        BinaryFormatter bF = new BinaryFormatter();
        bF.Serialize(fS, testData);
        fS.Close();
    }

    public static Data Load()
    {
        string path = Application.persistentDataPath + "/test.save";

        if (File.Exists(path))
        {
            FileStream fS = new FileStream(path, FileMode.Open);
            BinaryFormatter bF = new BinaryFormatter();
            Data testData = (Data)bF.Deserialize(fS);
            fS.Close();
            return testData;
        } else return null;
    }
}
